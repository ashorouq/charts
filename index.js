(async function () {

  //Dataset
  const seriesData = await fetchSeriesData();
  console.log('seriesData: ', seriesData);
  let dataSets = seriesData;

    function draw() {

      //Canva
      const canva = document.getElementById('kline'); 
      const context = canva.getContext('2d');
      const contextWidth = canva.width - 10;
      const contextHeight = canva.height;            
      context.clearRect(0, 0, contextWidth, contextHeight);
   
      //HighestPrice and lowestPrice
      //[time, open, high, low, close]
      let highestPrice =  0;
      let dataSlice1 = dataSets[0].slice(1, 5);
      const toNum = dataSlice1.map((i) => Number(i));
      let lowestPrice = toNum[0];
      for (let data of dataSets) {        
        let dataSlice = data.slice(1, 5);
        let toNum = dataSlice.map((i) => Number(i));
        max = Math.max(...toNum); 
        highestPrice = max > highestPrice ? max : highestPrice; 
        min = Math.min(...toNum); 
        lowestPrice = min < lowestPrice ? min : lowestPrice;  
      }

      //Y axis 
      context.beginPath();
      let xWidth= contextWidth * 0.97;
      context.moveTo(xWidth,  contextHeight);
      context.lineTo(xWidth, 0);
      context.strokeStyle = 'grey';
      context.stroke();
      const diff = highestPrice - lowestPrice; 
      const avg = diff / contextHeight;   
      let text;
      let label =  Math.floor(lowestPrice);
      const steps = Math.floor(diff / 100);
      let index = 1
      while (index > 0) {
        context.beginPath();
        context.fillStyle = "grey";
        context.moveTo(xWidth, contextHeight * index);
        context.lineTo(xWidth, contextHeight * index);
        context.stroke();
        text = String(label.toLocaleString("en", {useGrouping: false, minimumFractionDigits: 2}));
        context.fillText(text, xWidth, contextHeight * index + 1);
        label += steps;
        index -= 0.10;
      }
      
      //CandleStick
      //[time, open, high, low, close]
      let xAxis = 0, yAxis;
      let height, width = 10;
      let color;
      for (let dataSet of dataSets) {
        let dataSlice = dataSet.slice(1, 5);
        let data = dataSlice.map((i) => Number(i));
        color = data[0] < data[3] ? 'green' : 'red';
        yAxis =  contextHeight - ((data[0] - lowestPrice) / avg);
        height = ((data[0] - data[3]) / diff) * contextHeight;
        context.fillStyle = color
        context.fillRect(xAxis, yAxis, width, height);
        context.beginPath();
        context.moveTo(xAxis + 5, contextHeight - ((data[1] - lowestPrice) / avg));
        context.lineTo(xAxis + 5, contextHeight - ((data[2] - lowestPrice) / avg));
        context.strokeStyle = color;
        context.stroke();
        xAxis += width;
      }
       context.closePath();
    }


    function subcribe(success) {
      try {
        const socket = new WebSocket('wss://stream.binance.com/stream?streams=btcusdt@kline_1m');
        socket.onmessage = e => {
        const res = JSON.parse(e.data);
        const { t, o, h, l, c } = res.data.k;
        success([t, o, h, l, c]);
        }
      } catch(e) {
        console.error(e.message);
      }
    }

    subcribe(data => { // data: [time, open, high, low, close]
      console.log('suncribe: ', data);
      console.log('suncribe: ', data[0])
      console.log('suncribe: ', dataSets[dataSets.length - 1][0])
      if(dataSets[dataSets.length - 1][0] === data[0]){
        dataSets.pop()
    }
      dataSets.push(data)
      dataSets = dataSets.slice(dataSets.length-124)
      draw();
    })

  // [time, open, high, low, close][]
  function fetchSeriesData() {
    return new Promise((resolve, reject) => {
      fetch('https://www.binance.com/api/v1/klines?symbol=BTCUSDT&interval=1m')
        .then(async res => {
          const data = await res.json();
          const result = data.map(([time, open, high, low, close]) => [time, open, high, low, close]);
          resolve(result);
        })
        .catch(e => reject(e));
    })
  }

})()